locals {
  rg                        = "${var.project}-kv-rg"
  kv                        = "${var.project}-kv"
  location                  = "uksouth"
  key_vault_access_policies = []
  secrets                   = []
}
