resource "azurerm_resource_group" "key_vault_rg" {
  name     = local.rg
  location = local.location
}

resource "azurerm_key_vault" "kv" {
  name                        = local.kv
  location                    = azurerm_resource_group.key_vault_rg.location
  resource_group_name         = azurerm_resource_group.key_vault_rg.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id           = data.azurerm_client_config.current.tenant_id
    object_id           = data.azurerm_client_config.current.object_id
    key_permissions     = ["Get", "List"]
    secret_permissions  = ["Get", "List"]
    storage_permissions = ["Get", "List"]
  }
}

locals {
  key_vault_secrets = [
    {
      key   = "tenantId"
      value = data.azurerm_client_config.current.tenant_id
    },
    {
      key   = "subscriptionId"
      value = data.azurerm_client_config.current.subscription_id
    }
  ]
}

resource "azurerm_key_vault_secret" "example" {
  count        = length(local.key_vault_secrets)
  name         = local.key_vault_secrets[count.index].key
  value        = local.key_vault_secrets[count.index].value
  key_vault_id = azurerm_key_vault.kv.id
}
